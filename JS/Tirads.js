const form = document.querySelector('form');
const result = document.querySelector('#result');

form.addEventListener('submit', function(e) {
  e.preventDefault();
  const size = parseInt(document.querySelector('#size').value);
  const echogenicity = parseInt(document.querySelector('#echogenicity').value);
  const contour = parseInt(document.querySelector('#contour').value);
  const calcifications = parseInt(document.querySelector('#calcifications').value);

  if (size >= 10 && size <= 20) {
    let risk = 0;
    if (echogenicity === 1) {
      risk += 2;
    } else if (echogenicity === 2) {
      risk += 1;
    }
    if (contour === 2) {
      risk += 2;
    } else if (contour === 3) {
      risk += 3;
    }
    if (calcifications === 2) {
      risk += 2;
    }
    result.innerHTML = `Риск злокачественного узла: ${risk}%`;
  } else if (size > 20) {
    result.innerHTML = `Риск злокачественного узла: 95%`;
  } else {
    result.innerHTML = `Риск злокачественного узла: 5%`;
  }
});

