<?php
$servername = "localhost";
$username = "username";
$password = " root@localhost";
$dbname = "fuelprice";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT type, price FROM fuel_prices";
$result = $conn->query($sql);

$prices = [];
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $prices[$row["type"]] = $row["price"];
  }
} else {
  echo "0 results";
}
$conn->close();

echo json_encode($prices);
?>
