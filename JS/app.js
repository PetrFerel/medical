function auth() {
  // let name = document.getElementById("name").value;
  let login = document.getElementById("login").value;
  let Passwords = document.getElementById("passwords").value;

  if (login != "" && Passwords != "") {
    window.location.assign("medical.html");
    alert("Login OK");
  } else {
    alert("Invalid enter your login ");
    return;
  }
}
//   next page js

let calculate = document.querySelector(".calculate");
// calculate.addEventListener('click', findBIM); change function click()

function findBIM() {
  let height = document.querySelector("#height").value;
  let weight = document.querySelector("#weight").value;

  if (height <= 0 || weight <= 0 || isNaN(height) || isNaN(weight)) {
    alert("Please fill all fields with valid numbers");
    return;
  }
  height = height / 100;
  let bmi = weight / (height * height);
  showResult(bmi);
}
function showResult(bmi) {
  let result = document.querySelector(".result");
  let bmiStatus;
  if (bmi < 18.5) {
    bmiStatus = "Underweight";
  } else if (bmi < 25) {
    bmiStatus = "Normal";
  } else if (bmi < 30) {
    bmiStatus = "Overweight";
  } else {
    bmiStatus = "Obese";
  }
  result.innerHTML = `<div class=${bmiStatus}>Your BMI is ${bmi.toFixed(
    2
  )} and you are ${bmiStatus}.</div>`;
}
